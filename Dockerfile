include(focal)

RUN apt install --yes apache2 php php-cgi libapache2-mod-php \
                      php-mbstring php-common php-pear
RUN apt install --yes ldap-account-manager
RUN cp -a /var/lib/ldap-account-manager/config /var/lib/ldap-account-manager/config.bak && \
    rm -f /var/lib/ldap-account-manager/config.bak/config.cfg && \
    cp /etc/ldap-account-manager/config.cfg /var/lib/ldap-account-manager/config.bak/
