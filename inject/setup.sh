#!/bin/bash -x

source /host/settings.sh

# init config files if needed
dir=/var/lib/ldap-account-manager
if [[ ! -f $dir/config/config.cfg ]]; then
    cp -a $dir/config.bak/* $dir/config/
    chown -R www-data:www-data $dir/config/
fi
rm -rf $dir/config.bak/

# fix apache2 config
a2ensite default-ssl
a2dissite 000-default
a2enmod ssl

# # limit the memory size of apache2
# sed -i /etc/apache2/mods-available/mpm_prefork.conf \
#     -e '/^<IfModule/,+5 s/StartServers.*/StartServers 2/' \
#     -e '/^<IfModule/,+5 s/MinSpareServers.*/MinSpareServers 2/' \
#     -e '/^<IfModule/,+5 s/MaxSpareServers.*/MaxSpareServers 4/' \
#     -e '/^<IfModule/,+5 s/MaxRequestWorkers.*/MaxRequestWorkers 50/'

# fix the DocumentRoot
sed -i /etc/apache2/sites-available/default-ssl.conf \
    -e 's#DocumentRoot.*#DocumentRoot /usr/share/ldap-account-manager#'
sed -i /etc/apache2/conf-available/ldap-account-manager.conf \
    -e '/Alias/d'

# restart apache2
systemctl restart apache2
