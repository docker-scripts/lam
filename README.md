# LDAP Account Manager

## Installation

  - First install `ds`: https://gitlab.com/docker-scripts/ds#installation

  - Then get the scripts: `ds pull lam`

  - Create a directory for the container: `ds init lam @lam.example.org`

  - Customize settings: `cd /var/ds/openldap/ ; vim settings.sh`

  - Make the container: `ds make`

## Other commands

```
ds stop
ds start
ds shell
ds help
```
